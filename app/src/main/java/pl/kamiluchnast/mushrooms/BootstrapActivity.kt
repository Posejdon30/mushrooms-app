package pl.kamiluchnast.mushrooms

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import dagger.hilt.android.AndroidEntryPoint
import pl.kamiluchnast.mushrooms.databinding.ActivityBootstrapBinding

@AndroidEntryPoint
class BootstrapActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ActivityBootstrapBinding>(this, R.layout.activity_bootstrap)
    }
}