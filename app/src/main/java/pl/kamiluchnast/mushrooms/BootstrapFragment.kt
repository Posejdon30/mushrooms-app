package pl.kamiluchnast.mushrooms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pl.kamiluchnast.mushrooms.databinding.FragmentBootstrapBinding
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import pl.kamiluchnast.mushrooms.utils.CryptoUtils
import pl.kamiluchnast.mushrooms.utils.TokenUtils
import javax.inject.Inject


@AndroidEntryPoint
class BootstrapFragment : Fragment() {

    @Inject
    lateinit var prefs: PreferenceWrapper

    @Inject
    lateinit var crypto: CryptoUtils

    @Inject
    lateinit var tokenUtils: TokenUtils

    private lateinit var binding: FragmentBootstrapBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentBootstrapBinding.inflate(inflater, container, false)
        context ?: return binding.root
//        val asymmetricKeyPair = crypto.getAsymmetricKeyPair()
        navController = findNavController()
        checkSavedLoginData()
        return binding.root
    }

    // @TODO BIOMETRIA
    private fun checkSavedLoginData() {
        if (prefs.username == null || !tokenUtils.isTokenOk(prefs.authToken)) {
            navController.navigate(R.id.action_bootstrap_to_login)
        } else {
            navController.navigate(R.id.action_bootstrap_to_friends)
        }
    }
}