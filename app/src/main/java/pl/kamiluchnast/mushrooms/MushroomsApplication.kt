package pl.kamiluchnast.mushrooms

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MushroomsApplication : Application()