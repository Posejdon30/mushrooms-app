package pl.kamiluchnast.mushrooms.authentication

import android.util.Log
import android.util.Patterns
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.authentication.data.LoginRepository
import pl.kamiluchnast.mushrooms.authentication.model.LoginFormState
import pl.kamiluchnast.mushrooms.authentication.model.Token
import pl.kamiluchnast.mushrooms.utils.Result

class LoginViewModel @ViewModelInject internal constructor(
    private val loginRepository: LoginRepository,
) : ViewModel() {

    private val _loginForm = MutableLiveData<LoginFormState>()
    val loginFormState: LiveData<LoginFormState> = _loginForm

    private fun isPasswordValid(password: String): Boolean {
        //@ TODO password strong regex validator
        //return password.matches(Regex(passwordRegex))
        return password.isNotBlank()
    }
    private val _loginResult = MutableLiveData<Token>()

    val loginResult: LiveData<Token> = _loginResult

    fun login(server: String, username: String, password: String) {
        viewModelScope.launch {
            val response = loginRepository.login(server, username, password)
            if (response is Result.Success) {
                _loginResult.postValue(response.data)
            }
            if (response is Result.Error) {
                response.exception.printStackTrace()
                Log.e("LOGIN VIEW MODEL", "${response.exception.message}")
                _loginForm.postValue(LoginFormState(loginError = response.exception.message))
            }
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = LoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = LoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = LoginFormState(isDataValid = true)
        }
    }

    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains('@')) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    companion object {
        private const val passwordRegex = """^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8}$"""
    }
}