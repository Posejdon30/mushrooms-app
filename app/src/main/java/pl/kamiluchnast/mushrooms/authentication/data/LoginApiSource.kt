package pl.kamiluchnast.mushrooms.authentication.data

import android.util.Log
import pl.kamiluchnast.mushrooms.authentication.model.LoginRequest
import pl.kamiluchnast.mushrooms.utils.Result.Error
import pl.kamiluchnast.mushrooms.utils.Result.Success
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject


class LoginApiSource @Inject constructor() {

    suspend fun login(server: String, username: String, password: String) = try {
        val api = Retrofit.Builder()
            .baseUrl(server)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(LoginWebService::class.java)
        Success(api.login(LoginRequest(username, password)))
    } catch (ex: Exception) {
        ex.printStackTrace()
        Log.e("LOGIN API SOURCE EXCEPTION", "${ex.message}")
        Error(ex)
    } catch (ex: RuntimeException) {
        ex.printStackTrace()
        Log.e("LOGIN API SOURCE EXCEPTION", "${ex.message}")
        Error(ex)
    }
}