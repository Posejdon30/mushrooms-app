package pl.kamiluchnast.mushrooms.authentication.data

import javax.inject.Inject


class LoginRepository @Inject constructor(private val webApi: LoginApiSource) {

    suspend fun login(server: String, username: String, password: String) = webApi.login(server, username, password)
}