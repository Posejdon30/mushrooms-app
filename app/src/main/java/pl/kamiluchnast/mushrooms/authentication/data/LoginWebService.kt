package pl.kamiluchnast.mushrooms.authentication.data

import pl.kamiluchnast.mushrooms.authentication.model.LoginRequest
import pl.kamiluchnast.mushrooms.authentication.model.Token
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

interface LoginWebService {

    @POST("/login")
    suspend fun login(@Body request: LoginRequest): Token
}