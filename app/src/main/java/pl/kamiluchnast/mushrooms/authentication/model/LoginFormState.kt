package pl.kamiluchnast.mushrooms.authentication.model

/**
 * Data validation state of the login form.
 */
data class LoginFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val loginError: String? = null,
    val isDataValid: Boolean = false
)