package pl.kamiluchnast.mushrooms.authentication.model

data class LoginRequest(val username: String, val password: String)