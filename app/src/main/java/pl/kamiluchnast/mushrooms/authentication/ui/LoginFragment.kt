package pl.kamiluchnast.mushrooms.authentication.ui

import android.app.AlertDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.authentication.LoginViewModel
import pl.kamiluchnast.mushrooms.databinding.FragmentLoginBinding
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import javax.inject.Inject

//@TODO dodać obsługę biometrii, mamy już w Passmanie
@AndroidEntryPoint
class LoginFragment : Fragment() {

    @Inject
    lateinit var prefs: PreferenceWrapper

    private val loginViewModel: LoginViewModel by viewModels()

    private lateinit var binding: FragmentLoginBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        navController = findNavController()
        context ?: return binding.root
        prepareInputs()
        observeLoginFormState()
        observeLoginResult()
        return binding.root
    }

    private fun prepareInputs() {
        binding.server.setText(prefs.server)
        binding.username.setText(prefs.username)
        binding.username.afterTextChanged {
            loginDataChanged()
        }
        binding.password.afterTextChanged {
            loginDataChanged()
        }
        binding.login.setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle("DEBUG")
                .setMessage("Logowanie testowe?")
                .setPositiveButton("TAK") { _, _ ->
                    prefs.saveLoginResponse(
                        binding.username.text.toString(),
                        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsImlzcyI6Im11c2hyb29tcyIsImV4cCI6MTYwODAxMTMzNSwiaWF0IjoxNjA3OTc1MzM1LCJyb2wiOlsiQURNSU4iLCJVU0VSIl19.qbf2H5dJvjJj4gU382Q19SxB9fM_kkz1YgufvTtX4EA",
                        binding.server.text.toString())
                    navController.navigate(R.id.action_login_to_friends)
                }.setNegativeButton("NIE") { _, _ ->
                    sendLoginRequest()
                }.show()
        }

        binding.password.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE ->
                    sendLoginRequest()
            }
            false
        }
    }

    private fun loginDataChanged() {
        loginViewModel.loginDataChanged(
            binding.username.text.toString(),
            binding.password.text.toString()
        )
    }

    private fun sendLoginRequest() {
        binding.login.isEnabled = false
        binding.loading.visibility = View.VISIBLE
        loginViewModel.login(
            binding.server.text.toString(),
            binding.username.text.toString(),
            binding.password.text.toString())
    }

    private fun observeLoginResult() {
        loginViewModel.loginResult.observe(viewLifecycleOwner, { token ->
            binding.loading.visibility = View.GONE
            prefs.saveLoginResponse(
                binding.username.text.toString(),
                token.token,
                binding.server.text.toString())
            navController.navigate(R.id.action_login_to_friends)
        })
    }

    private fun observeLoginFormState() {
        loginViewModel.loginFormState.observe(viewLifecycleOwner, Observer {
            val loginState = it ?: return@Observer

            binding.login.isEnabled = loginState.isDataValid
            if (loginState.usernameError != null) {
                binding.username.error = getString(loginState.usernameError)
            }
            if (loginState.loginError != null) {
                binding.username.error = loginState.loginError
            }
            if (loginState.passwordError != null) {
                binding.password.error = getString(loginState.passwordError)
            }
        })
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}