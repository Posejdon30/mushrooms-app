package pl.kamiluchnast.mushrooms.bluetooth

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import pl.kamiluchnast.mushrooms.bluetooth.data.BluetoothService
import pl.kamiluchnast.mushrooms.bluetooth.model.DeviceItem
import pl.kamiluchnast.mushrooms.bluetooth.ui.BluetoothListAdapter
import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendBtResult
import pl.kamiluchnast.mushrooms.friends.data.FriendsRepository
import pl.kamiluchnast.mushrooms.friends.data.db.Friend

class BluetoothViewModel @ViewModelInject constructor(
    private val bluetoothService: BluetoothService,
    private val friendsRepository: FriendsRepository
) : ViewModel() {

    private val _devices = MutableLiveData<MutableList<DeviceItem>>()
    val devices: LiveData<MutableList<DeviceItem>> = _devices
    private val _newFriendBtResult = MutableLiveData<NewFriendBtResult>()
    val newFriendBtResult: LiveData<NewFriendBtResult> = _newFriendBtResult
    var devicesViewAdapter: BluetoothListAdapter

    init {
        _devices.value = mutableListOf()
        devicesViewAdapter = BluetoothListAdapter(_devices.value!!, this)
    }

    fun startBluetoothService() {
        viewModelScope.launch {
            bluetoothService.getPairedDevices()?.forEach {
                appendToDeviceList(DeviceItem(it.name, it.address, it))
            }
            bluetoothService.startDiscovery(_newFriendBtResult)
        }
    }

    val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                BluetoothDevice.ACTION_FOUND -> {
                    val device: BluetoothDevice? = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    if (device != null && device.name != null && device.address != null) {
                        appendToDeviceList(DeviceItem(device.name, device.address, device))
                    }
                }
            }
        }
    }

    fun startConnection(device: BluetoothDevice, isSecond: Boolean = false) {
        bluetoothService.startConnection(device, isSecond)
    }

    fun appendToDeviceList(device: DeviceItem) {
        _devices.value?.filter { it.address == device.address }?.ifEmpty {
            //@TODO zmiana bez sprawdzenia - może generować błąd
            _devices.postValue(_devices.value.also { it?.add(device) })
        }
    }

    fun persistFriend(friendResult: NewFriendBtResult) {
        viewModelScope.launch {
            friendsRepository.addFriend(Friend(
                friendResult.username,
                friendResult.avatar,
                friendResult.isRemote,
                friendResult.remoteAddress,
                friendResult.publicKey),
                friendResult.myPassword!!, friendResult.remotePassword!!)
        }
    }
}