package pl.kamiluchnast.mushrooms.bluetooth.data

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.json.JSONObject
import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendBtResult
import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import pl.kamiluchnast.mushrooms.utils.CryptoUtils
import java.util.*
import java.util.concurrent.ExecutorService
import javax.inject.Inject

/*
    @TODO można dodać obsługę ponowienia 2-3 razy w przypadku błędu, uwaga na celowe
      zamykanie w przypadku uruchamiania klienta
*/
class BluetoothService @Inject constructor(
    private val executor: ExecutorService,
    private val crypto: CryptoUtils,
    private val prefs: PreferenceWrapper
) : ViewModel() {

    private lateinit var newFriendResult: MutableLiveData<NewFriendBtResult>
    private var bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private lateinit var bluetoothServer: BluetoothServerSocket
    private var appUuid: UUID
    private var newFriend: Friend? = null
    private val serverPassword: String

    companion object {
        private const val TAG_CLIENT = "MSH-BT CLIENT"
        private const val TAG_SERVER = "MSH-BT SERVER"
        private const val appName = "mushrooms"
    }

    init {
        appUuid = UUID.nameUUIDFromBytes(appName.toByteArray())
        serverPassword = generateRandomPasswordForServersAuth()
    }

    // @TODO security refactor needed
    private fun generateRandomPasswordForServersAuth() = UUID.randomUUID().toString()

    fun getPairedDevices() = bluetoothAdapter.bondedDevices?.filter {
        BluetoothDevice.DEVICE_TYPE_CLASSIC == it.type
    }

    fun startDiscovery(_newFriendResult: MutableLiveData<NewFriendBtResult>) {
        newFriendResult = _newFriendResult
        startBtServer()
        bluetoothAdapter.startDiscovery()
    }

    fun startConnection(device: BluetoothDevice, isSecond: Boolean = false) {
        Log.i(TAG_CLIENT, "connecting to device: ${device.name}")
        executor.execute {
            runCatching {
                runCatching { bluetoothServer.close() }
                bluetoothAdapter.cancelDiscovery()
                val clientSocket = device.createRfcommSocketToServiceRecord(appUuid)
                clientSocket.connect()
                Log.i(TAG_CLIENT, "connected to server")
                clientSocket.use { sendData(clientSocket, buildMessage(), TAG_CLIENT) }
                if (!isSecond) {
                    Log.i(TAG_SERVER, "second connection")
                    executor.execute {
                        startBtServer(true)
                    }
                }
            }.onSuccess { Result.success(newFriend) }
                .onFailure {
                    Log.e(TAG_CLIENT, "failure on client", it)
                    Result.failure<Any>(it)
                }
        }
    }

    private fun startBtServer(isSecond: Boolean = false) {
        Log.i(TAG_SERVER, "server ready for connections")
        runCatching {
            bluetoothServer = bluetoothAdapter.listenUsingRfcommWithServiceRecord(appName, appUuid)
            val serverSocket = bluetoothServer.accept()
            Log.i(TAG_SERVER, "device connected: ${serverSocket.remoteDevice.name}")
            bluetoothAdapter.cancelDiscovery()
            serverSocket.use { receiveData(it, TAG_SERVER) }
            if (!isSecond) {
                Log.i(TAG_CLIENT, "second connection")
                startConnection(serverSocket.remoteDevice, true)
            }
        }.onFailure { Log.w(TAG_SERVER, "failure on server or server closed ${it.message}") }
    }

    private fun sendData(socket: BluetoothSocket, message: String, tag: String) {
        Thread.sleep(2000)
        Log.i(tag, "sending data")
        runCatching {
            val writer = socket.outputStream.bufferedWriter()
            writer.use {
                var dataSent = false
                while (!dataSent) {
                    runCatching {
                        it.write(message)
                        dataSent = true
                        Log.i(tag, "data sent")
                    }.onFailure { Log.e(tag, "failure on sending", it) }
                }

            }
        }.onFailure { Log.e(tag, "failure on sending 2", it) }
    }

    private fun buildMessage(): String {
        return JSONObject()
            .put("username", prefs.username)
            .put("server_address", prefs.server)
            .put("user_public_key", crypto.keyPair!!.public.encoded.contentToString())
            .put("server_password", serverPassword)
            .toString(2)
    }

    private fun receiveData(socket: BluetoothSocket, tag: String) {
        val reader = socket.inputStream.reader()
        reader.use {
            Log.i(tag, "waiting for data")
            var keepReading = true
            val buffer = CharArray(102400)
            while (keepReading) {
                if (it.read(buffer) != -1) {
                    keepReading = false
                    val receivedData = JSONObject(String(buffer))
                    Log.i(tag, "data received: $receivedData")
                    postReceivedFriend(receivedData)
                }
            }
            reader.close()
        }
    }

    private fun postReceivedFriend(receivedData: JSONObject) {
        newFriendResult.postValue(
            NewFriendBtResult(
                receivedData.getString("username"),
                avatar = null,
                isRemote = true,
                remoteAddress = receivedData.getString("server_address"),
                myPassword = receivedData.getString("server_password"),
                remotePassword = serverPassword,
                publicKey = crypto.parsePublicKey(receivedData.getString("user_public_key")).toString(),
            )
        )
    }
}