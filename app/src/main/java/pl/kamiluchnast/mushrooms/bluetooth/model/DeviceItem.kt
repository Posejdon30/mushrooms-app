package pl.kamiluchnast.mushrooms.bluetooth.model

import android.bluetooth.BluetoothDevice

data class DeviceItem(
    val name: String,
    val address: String,
    val device: BluetoothDevice? = null,
    var connectionState: String? = null
)
