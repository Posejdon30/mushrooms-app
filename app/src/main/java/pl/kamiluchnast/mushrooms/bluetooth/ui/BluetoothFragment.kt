package pl.kamiluchnast.mushrooms.bluetooth.ui

import android.bluetooth.BluetoothDevice
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import pl.kamiluchnast.mushrooms.bluetooth.BluetoothViewModel
import pl.kamiluchnast.mushrooms.databinding.FragmentBluetoothBinding

@AndroidEntryPoint
class BluetoothFragment : Fragment() {

    private val bluetoothViewModel: BluetoothViewModel by viewModels()

    private lateinit var binding: FragmentBluetoothBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentBluetoothBinding.inflate(inflater, container, false)
        context ?: return binding.root
        navController = findNavController()
        recyclerSetup()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        requireActivity().parent.registerReceiver(bluetoothViewModel.receiver, IntentFilter(BluetoothDevice.ACTION_FOUND))
        bluetoothViewModel.startBluetoothService()
        observeNewFriendResult()
    }

    private fun recyclerSetup() {
        binding.bluetoothDevices.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = bluetoothViewModel.devicesViewAdapter
        }

        bluetoothViewModel.devices.observe(viewLifecycleOwner, Observer {
            Log.i("BT ACTIVITY", "devices list changed")
            binding.bluetoothDevices.adapter?.notifyDataSetChanged()
        })
    }

    private fun observeNewFriendResult() {
        bluetoothViewModel.newFriendBtResult.observe(this@BluetoothFragment, Observer {
            Log.i("FRIENDS FRAGMENT", "friend received: ${it?.username}")
            bluetoothViewModel.persistFriend(it)
            BluetoothFragmentDirections.actionBluetoothToMessages(it.username)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().parent.unregisterReceiver(bluetoothViewModel.receiver)
    }
}