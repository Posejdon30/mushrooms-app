package pl.kamiluchnast.mushrooms.bluetooth.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.bluetooth.BluetoothViewModel
import pl.kamiluchnast.mushrooms.bluetooth.model.DeviceItem

class BluetoothListAdapter(
    private val devices: MutableList<DeviceItem>,
    private val bluetoothViewModel: BluetoothViewModel
) : RecyclerView.Adapter<BluetoothListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.bluetooth_device_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = devices.elementAt(position)
        holder.deviceName.text = item.name
        holder.macAddress.text = item.address
        holder.device = item.device
        holder.connectBtn.setOnClickListener {
            holder.device?.let {
                bluetoothViewModel.startConnection(it)
            }
        }
    }

    override fun getItemCount(): Int = devices.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val deviceName: TextView = view.findViewById(R.id.deviceName)
        val macAddress: TextView = view.findViewById(R.id.macAddress)
        val connectBtn: Button = view.findViewById(R.id.btConnectBtn)
        var device: android.bluetooth.BluetoothDevice? = null

        override fun toString(): String {
            return super.toString() + " {name: ${deviceName.text}, address: ${macAddress.text}"
        }
    }
}