package pl.kamiluchnast.mushrooms.conversation

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.launch
import pl.kamiluchnast.mushrooms.conversation.data.MessagesRepository
import pl.kamiluchnast.mushrooms.conversation.data.db.Message
import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import pl.kamiluchnast.mushrooms.friends.data.db.FriendsLocalDataSource

@ExperimentalPagingApi
class MessagesViewModel @ViewModelInject constructor(
    private val messagesRepository: MessagesRepository,
    private val friendsDbSource: FriendsLocalDataSource,
) : ViewModel() {

    private lateinit var friend: Friend

    suspend fun liveMessages(friendUsername: String): LiveData<PagingData<Message>> {
        friend = friendsDbSource.getFriend(friendUsername)
        return messagesRepository
            .allMessagesWith(friend)
            .distinctUntilChanged()
            .cachedIn(viewModelScope)
    }

    fun sendMessage(message: Message) {
        viewModelScope.launch {
            messagesRepository.sendMessage(message, friend)
        }
    }

    fun sendMessageFromFriend(message: Message) {
        viewModelScope.launch {
            messagesRepository.sendMessageFromFriend(message)
        }
    }
}