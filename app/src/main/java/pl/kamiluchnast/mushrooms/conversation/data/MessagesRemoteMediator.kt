package pl.kamiluchnast.mushrooms.conversation.data

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import pl.kamiluchnast.mushrooms.conversation.data.MessagesRepository.Companion.DEFAULT_PAGE_INDEX
import pl.kamiluchnast.mushrooms.conversation.data.db.*
import pl.kamiluchnast.mushrooms.conversation.data.web.MessagesApi
import javax.inject.Inject


@ExperimentalPagingApi
class MessagesRemoteMediator @Inject constructor
    (private val messagesApi: MessagesApi, private val messagesDatabase: MessagesDatabase) :
    RemoteMediator<Int, Message>() {

    private val messagesDao: MessagesDao = messagesDatabase.messagesDao()
    private val remoteKeysDao: RemoteKeysDao = messagesDatabase.remoteKeysDao()
    var friendUsername: String = ""

    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, Message>,
    ): MediatorResult {
        try {
            val pageRemoteKey = getPageRemoteKey(loadType, state) ?: return MediatorResult.Success(endOfPaginationReached = true)
            val pagedResponse = messagesApi.getMyConversationWith(friendUsername, "created,desc", pageRemoteKey, state.config.pageSize)
            messagesDatabase.withTransaction {
                clearDbOnRefresh(loadType)
                val messages = pagedResponse.content.sortedByDescending { it.created }
                val prevKey = if (pagedResponse.first) null else pageRemoteKey - 1
                val nextKey = if (pagedResponse.last) null else pageRemoteKey + 1
                val keys = messages.map {
                    RemoteKeys(messageId = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                remoteKeysDao.insertAll(keys)
                messagesDao.insertAll(messages)
            }
            return MediatorResult.Success(pagedResponse.last)
        } catch (e: Exception) {
            return MediatorResult.Error(e)
        } catch (re: RuntimeException) {
            return MediatorResult.Error(re)
        }
    }

    private suspend fun getPageRemoteKey(loadType: LoadType, state: PagingState<Int, Message>): Int? {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKey = getClosestRemoteKey(state)
                remoteKey?.nextKey?.minus(1) ?: DEFAULT_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKey = getFirstRemoteKey(state)
                if (remoteKey == null) 0 else remoteKey.nextKey

            }
            LoadType.APPEND -> {
                val remoteKey = getLastRemoteKey(state)
                if (remoteKey == null) 0 else remoteKey.prevKey
            }
        }
    }

    private suspend fun getLastRemoteKey(state: PagingState<Int, Message>): RemoteKeys? {
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()?.let {
                message -> remoteKeysDao.remoteKeysMessageId(message.id) }
    }

    private suspend fun getFirstRemoteKey(state: PagingState<Int, Message>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()?.let {
                message -> remoteKeysDao.remoteKeysMessageId(message.id) }
    }

    private suspend fun getClosestRemoteKey(state: PagingState<Int, Message>): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                remoteKeysDao.remoteKeysMessageId(repoId)
            }
        }
    }

    private suspend fun clearDbOnRefresh(loadType: LoadType) {
        if (loadType == LoadType.REFRESH) {
            remoteKeysDao.clearRemoteKeys()
            messagesDao.clearAll()
        }
    }

}
