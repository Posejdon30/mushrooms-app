package pl.kamiluchnast.mushrooms.conversation.data

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.paging.*
import pl.kamiluchnast.mushrooms.conversation.data.db.Message
import pl.kamiluchnast.mushrooms.conversation.data.db.MessagesDatabase
import pl.kamiluchnast.mushrooms.conversation.data.web.MessagesApi
import pl.kamiluchnast.mushrooms.conversation.data.web.rest.MessageRequest
import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import pl.kamiluchnast.mushrooms.utils.CryptoUtils
import javax.inject.Inject

@ExperimentalPagingApi
class MessagesRepository @Inject constructor(
    database: MessagesDatabase,
    private val remoteApi: MessagesApi,
    private val remoteMediator: MessagesRemoteMediator,
    private val crypto: CryptoUtils,
) {

    private val messagesDao = database.messagesDao()
    private lateinit var pager: Pager<Int, Message> // @TODO czy to jest na pewno potrzebne w polach?

    suspend fun sendMessage(message: Message, friend: Friend) {
        val encryptedMessage = message.apply {
            body = crypto.encryptWithKey(body, crypto.parsePublicKey(friend.publicKey!!))
        }
        runCatching { messagesDao.sendMessage(encryptedMessage) }
            .onFailure { Log.e("MESSAGE REPO", "local db error: ${it.message}") }
        remoteApi.sendMessage(MessageRequest(encryptedMessage))
    }

    suspend fun sendMessageFromFriend(message: Message) {
        val encryptedMessage = message.apply {
            body = crypto.encryptWithKey(body, crypto.keyPair.public)
        }
        runCatching { messagesDao.sendMessage(encryptedMessage) }
            .onFailure { Log.e("MESSAGE REPO", "local db error: ${it.message}") }
    }

    fun allMessagesWith(friend: Friend, pagingConfig: PagingConfig = getDefaultPageConfig()):
            LiveData<PagingData<Message>> {
        remoteMediator.friendUsername = friend.username
        pager = Pager(pagingConfig, 0, remoteMediator, {
            messagesDao.allMessagesWith(friend.username)
        })
        return pager.liveData.map { page ->
            page.map {
                it.body = crypto.decrypt(it.body)
                it
            }
        }
    }

    private fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(
            pageSize = DEFAULT_PAGE_SIZE,
            enablePlaceholders = true,
            prefetchDistance = DEFAULT_PREFETCH_DISTANCE,
            initialLoadSize = INITIAL_LOAD_SIZE)
    }

    companion object {
        const val DEFAULT_PAGE_INDEX = 0
        const val DEFAULT_PAGE_SIZE = 5
        const val DEFAULT_PREFETCH_DISTANCE = 5
        const val INITIAL_LOAD_SIZE = 30
    }
}