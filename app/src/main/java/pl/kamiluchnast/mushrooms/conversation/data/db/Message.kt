package pl.kamiluchnast.mushrooms.conversation.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.Instant
import java.util.*

@Entity(tableName = "messages")
data class Message @JvmOverloads constructor(
    @PrimaryKey @ColumnInfo(name = "id") val id: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "from") var from: String = "",
    @ColumnInfo(name = "to") var to: String = "",
    @ColumnInfo(name = "body") var body: String = "",
    @ColumnInfo(name = "created") var created: Long = Instant.now().toEpochMilli(),
    @ColumnInfo(name = "isImage") var isImage: Boolean = false,
)
