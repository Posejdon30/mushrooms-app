package pl.kamiluchnast.mushrooms.conversation.data.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MessagesDao {

    @Query("SELECT * FROM messages WHERE `from` = :friendUsername OR `to` = :friendUsername ORDER BY `created` ASC")
    fun allMessagesWith(friendUsername: String): PagingSource<Int, Message>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun sendMessage(message: Message)

    // for Paging
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(users: List<Message>)

    @Query("DELETE FROM messages")
    suspend fun clearAll()
}