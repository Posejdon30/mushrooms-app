package pl.kamiluchnast.mushrooms.conversation.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Message::class, RemoteKeys::class], version = 1, exportSchema = true)
abstract class MessagesDatabase : RoomDatabase() {

    abstract fun messagesDao(): MessagesDao

    abstract fun remoteKeysDao(): RemoteKeysDao
}
