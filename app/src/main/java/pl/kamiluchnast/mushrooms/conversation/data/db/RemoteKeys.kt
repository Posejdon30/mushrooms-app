package pl.kamiluchnast.mushrooms.conversation.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class RemoteKeys(@PrimaryKey val messageId: String, val prevKey: Int?, val nextKey: Int?)
