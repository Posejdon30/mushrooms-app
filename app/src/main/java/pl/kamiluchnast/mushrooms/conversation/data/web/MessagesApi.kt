package pl.kamiluchnast.mushrooms.conversation.data.web

import pl.kamiluchnast.mushrooms.conversation.data.db.Message
import pl.kamiluchnast.mushrooms.conversation.data.web.rest.MessageRequest
import pl.kamiluchnast.mushrooms.conversation.data.web.rest.MessageResponse
import pl.kamiluchnast.mushrooms.utils.Pageable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface MessagesApi {
    @GET("/api/messages")
    suspend fun getMyConversationWith(
        @Query("with_friend") friendUsername: String,
        @Query("sort") sort: String,
        @Query("page") page: Int,
        @Query("size") size: Int,
    ): Pageable<Message>

    @POST("/api/messages")
    suspend fun sendMessage(@Body request: MessageRequest): MessageResponse?
}