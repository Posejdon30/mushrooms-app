package pl.kamiluchnast.mushrooms.conversation.data.web.rest

import pl.kamiluchnast.mushrooms.conversation.data.db.Message

data class MessageRequest(val to: String, val body: String, val isImage: Boolean) {

    constructor(message: Message) : this(message.to, message.body, message.isImage)
}
