package pl.kamiluchnast.mushrooms.conversation.data.web.rest

data class MessageResponse(val status: String?, val desc: String?)
