package pl.kamiluchnast.mushrooms.conversation.ui

import android.app.Activity
import android.content.Intent
import android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.conversation.MessagesViewModel
import pl.kamiluchnast.mushrooms.conversation.data.db.Message
import pl.kamiluchnast.mushrooms.databinding.FragmentMessagesBinding
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import javax.inject.Inject


@ExperimentalPagingApi
@AndroidEntryPoint
class MessagesFragment : Fragment() {

    @Inject
    lateinit var prefs: PreferenceWrapper

    private val args: MessagesFragmentArgs by navArgs()
    private val viewModel: MessagesViewModel by viewModels()
    private val messagesAdapter: MessagesListAdapter = MessagesListAdapter()

    private lateinit var binding: FragmentMessagesBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMessagesBinding.inflate(inflater, container, false)
        context ?: return binding.root
        navController = findNavController()
        activity?.title = args.friendUsername
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        initMessageList()
        initBottomBar()
        initBottomBarFromFriend()
    }

    private fun initBottomBar() {
        binding.sendBtn.setOnClickListener {
            viewModel.sendMessage(
                Message(
                    from = prefs.username!!,
                    to = args.friendUsername,
                    body = binding.messageText.text.toString()
                )
            )
            binding.messageText.text.clear()
            messagesAdapter.notifyDataSetChanged()
        }
        binding.cameraBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            intent.type = "image/*"
            intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            startActivityForResult(Intent.createChooser(intent, getString(R.string.prompt_pick_photo)), 1);
        }
        binding.messageText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.messageText.text.clear()
            } else {
                binding.messageText.setText(R.string.type_message)
            }
        }
    }

    private fun initBottomBarFromFriend() {
        binding.sendBtnFF.setOnClickListener {
            viewModel.sendMessageFromFriend(
                Message(
                    from = args.friendUsername,
                    to = prefs.username!!,
                    body = binding.messageTextFF.text.toString()
                )
            )
            binding.messageTextFF.text.clear()
            messagesAdapter.notifyDataSetChanged()
        }
        binding.cameraBtnFF.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
            intent.type = "image/*"
            intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true)
            startActivityForResult(Intent.createChooser(intent, getString(R.string.prompt_pick_photo)), 2);
        }
        binding.messageTextFF.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.messageTextFF.text.clear()
            } else {
                binding.messageTextFF.setText(R.string.type_message)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, result: Intent?) {
        super.onActivityResult(requestCode, resultCode, result)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            activity?.contentResolver?.openInputStream(result?.data!!).use {
                viewModel.sendMessage(Message(
                    from = prefs.username!!,
                    to = args.friendUsername,
                    body = Base64.encodeToString(it?.readBytes(), Base64.NO_WRAP),
                    isImage = true
                ))
            }
        }
        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            activity?.contentResolver?.openInputStream(result?.data!!).use {
                viewModel.sendMessageFromFriend(Message(
                    from = args.friendUsername,
                    to = prefs.username!!,
                    body = Base64.encodeToString(it?.readBytes(), Base64.NO_WRAP),
                    isImage = true
                ))
            }
        }
    }

    private fun initMessageList() {
        messagesAdapter.me = prefs.username!!

        binding.messagelist.apply {
            setHasFixedSize(true)
            adapter = messagesAdapter
            val layout = LinearLayoutManager(context)
            layout.stackFromEnd = true
            layoutManager = layout
        }

        lifecycleScope.launch {
            viewModel.liveMessages(args.friendUsername).observe(viewLifecycleOwner, {
                messagesAdapter.submitData(lifecycle, it)
            })
        }
    }
}