package pl.kamiluchnast.mushrooms.conversation.ui

import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.conversation.data.db.Message
import java.io.ByteArrayInputStream
import java.time.Instant
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter


class MessagesListAdapter() : PagingDataAdapter<Message, MessagesListAdapter.MessageViewHolder>(COMPARATOR) {

    lateinit var me: String

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val item = getItem(position) ?: return
        holder.created.text = dateFormatter.format(Instant.ofEpochMilli(item.created))
        if (item.from == me) {
            showMyMessage(holder, item)
        } else {
            showFriendMessage(holder, item)
        }
    }

    private fun showMyMessage(holder: MessageViewHolder, item: Message) {
        holder.myLayout.visibility = View.VISIBLE
        holder.friendLayout.visibility = View.GONE
        if (item.isImage) {
            holder.myMessage.visibility = View.GONE
            showImage(item, holder.myImage)
            return
        } else {
            holder.myImage.visibility = View.GONE
            holder.myMessage.visibility = View.VISIBLE
            holder.myMessage.text = item.body
        }
    }

    private fun showFriendMessage(holder: MessageViewHolder, item: Message) {
        holder.myLayout.visibility = View.GONE
        holder.friendLayout.visibility = View.VISIBLE
        if (item.isImage) {
            holder.friendMessage.visibility = View.GONE
            showImage(item, holder.friendImage)
        } else {
            holder.friendImage.visibility = View.GONE
            holder.friendMessage.visibility = View.VISIBLE
            holder.friendMessage.text = item.body
        }
    }

    private fun showImage(item: Message, image: ImageView) {
        try {
            image.setImageBitmap(BitmapFactory.decodeStream(ByteArrayInputStream(Base64.decode(item.body, Base64.NO_WRAP))))
            image.visibility = ImageView.VISIBLE
            } catch (e: IllegalArgumentException) {
            Log.e("MESSAGE_FRAG", "image data corrupted", e)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MessageViewHolder.create(parent)

    companion object {
        // @TODO catch user time zone for displaying correct time
        val dateFormatter: DateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneOffset.UTC)
        private val COMPARATOR = object : DiffUtil.ItemCallback<Message>() {
            override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }

    class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val friendLayout: LinearLayout = itemView.findViewById(R.id.friend_layout)
        val myLayout: LinearLayout = itemView.findViewById(R.id.my_layout)
        val created: TextView = itemView.findViewById(R.id.created)
        val friendMessage: TextView = itemView.findViewById(R.id.friend_message)
        val myMessage: TextView = itemView.findViewById(R.id.my_message)
        val friendImage: ImageView = itemView.findViewById(R.id.friend_image)
        val myImage: ImageView = itemView.findViewById(R.id.my_image)
        val friendAvatar: ImageView = itemView.findViewById(R.id.friend_avatar)
        val myAvatar: ImageView = itemView.findViewById(R.id.my_avatar)

        override fun toString(): String {
            return super.toString() + " $created $friendMessage $myMessage"
        }

        companion object {
            fun create(parent: ViewGroup): MessageViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.message_item, parent, false)
                return MessageViewHolder(view)
            }
        }
    }
}
