package pl.kamiluchnast.mushrooms.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import pl.kamiluchnast.mushrooms.friends.data.db.FriendsDbSource
import pl.kamiluchnast.mushrooms.friends.data.db.FriendsLocalDataSource
import pl.kamiluchnast.mushrooms.friends.data.web.FriendsRemoteDataSource
import pl.kamiluchnast.mushrooms.friends.data.web.FriendsWebSource

@InstallIn(ApplicationComponent::class)
@Module
abstract class FriendsDataSourceModule {

    @Binds
    abstract fun bindFriendsDbSource(impl: FriendsDbSource): FriendsLocalDataSource

    @Binds
    abstract fun bindFriendsWebSource(impl: FriendsWebSource): FriendsRemoteDataSource
}