package pl.kamiluchnast.mushrooms.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.Request
import pl.kamiluchnast.mushrooms.friends.data.db.FriendsDao
import pl.kamiluchnast.mushrooms.friends.data.db.FriendsDatabase
import pl.kamiluchnast.mushrooms.friends.data.web.FriendsApiService
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.Duration
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object FriendsModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): FriendsDatabase {
        return Room.databaseBuilder(
            appContext, FriendsDatabase::class.java, "friends.db"
        ).build()
    }

    @Provides
    fun provideFriendsDao(database: FriendsDatabase): FriendsDao {
        return database.friendDao()
    }

    @Provides
    @Singleton
    fun bindFriendsWebService(prefs: PreferenceWrapper): FriendsApiService {
        val okHttpClient = OkHttpClient().newBuilder()
            .addInterceptor { chain ->
                val original: Request = chain.request()
                val request: Request = original.newBuilder()
                    .header("Authorization", prefs.authToken!!)
                    .method(original.method, original.body)
                    .build()
                chain.proceed(request)
            }
            .connectTimeout(Duration.ofMinutes(1))
            .readTimeout(Duration.ofMinutes(1))
            .callTimeout(Duration.ofMinutes(1))
            .build()
        return Retrofit.Builder().baseUrl(prefs.server)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(FriendsApiService::class.java)
    }
}