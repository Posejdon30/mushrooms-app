package pl.kamiluchnast.mushrooms.di

import android.util.Log
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import pl.kamiluchnast.mushrooms.authentication.data.LoginWebService
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object LoginModule {

    @Provides
    @Singleton
    fun bindLoginWebService(prefs: PreferenceWrapper): LoginWebService {
        Log.w("LOGIN MODULE", "retrofit URL SET TO ${prefs.server}")
        return Retrofit.Builder()
            .baseUrl(prefs.server)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(LoginWebService::class.java)
    }
}