package pl.kamiluchnast.mushrooms.di

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import pl.kamiluchnast.mushrooms.conversation.data.MessagesRemoteMediator
import pl.kamiluchnast.mushrooms.conversation.data.db.MessagesDao
import pl.kamiluchnast.mushrooms.conversation.data.db.MessagesDatabase
import pl.kamiluchnast.mushrooms.conversation.data.db.RemoteKeysDao
import pl.kamiluchnast.mushrooms.conversation.data.web.MessagesApi
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@InstallIn(ApplicationComponent::class)
@Module
object MessagesModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext appContext: Context): MessagesDatabase {
        return Room.databaseBuilder(
            appContext, MessagesDatabase::class.java, "messages.db"
        ).fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideLogDao(database: MessagesDatabase): MessagesDao {
        return database.messagesDao()
    }

    @Provides
    fun provideRemoteKeysDao(database: MessagesDatabase): RemoteKeysDao {
        return database.remoteKeysDao()
    }

    @ExperimentalPagingApi
    @Provides
    fun provideMessageRemoteMediator(messagesApi: MessagesApi, messagesDatabase: MessagesDatabase):
            MessagesRemoteMediator {
        return MessagesRemoteMediator(messagesApi, messagesDatabase)
    }

    @Provides
    @Singleton
    fun bindMessagesWebService(prefs: PreferenceWrapper): MessagesApi {
        val logging = HttpLoggingInterceptor()
        // REQUEST LOGGING
        // logging.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient().newBuilder()
            .addInterceptor(logging)
            .addInterceptor { chain ->
                val original: Request = chain.request()
                val request: Request = original.newBuilder()
                    .header("Authorization", prefs.authToken!!)
                    .method(original.method, original.body)
                    .build()
                chain.proceed(request)
            }.build()
        return Retrofit.Builder().baseUrl(prefs.server)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(MessagesApi::class.java)
    }
}