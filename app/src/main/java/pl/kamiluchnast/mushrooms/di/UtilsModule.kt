package pl.kamiluchnast.mushrooms.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import pl.kamiluchnast.mushrooms.utils.PreferenceWrapper
import pl.kamiluchnast.mushrooms.utils.CryptoUtils
import pl.kamiluchnast.mushrooms.utils.TokenUtils
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Singleton

@InstallIn(ApplicationComponent::class)
@Module
object UtilsModule {

    @Provides
    @Singleton
    fun provideExecutorPool(): ExecutorService = Executors.newFixedThreadPool(4)

    @Provides
    @Singleton
    fun provideCryptoUtils(): CryptoUtils = CryptoUtils()

    @Provides
    @Singleton
    fun providePreferenceWrapper(@ApplicationContext appContext: Context): PreferenceWrapper = PreferenceWrapper(appContext)

    @Provides
    @Singleton
    fun provideIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    @Singleton
    fun provideTokenUtils() = TokenUtils()
}