package pl.kamiluchnast.mushrooms.friends

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendBtResult
import pl.kamiluchnast.mushrooms.friends.data.FriendsRepository
import pl.kamiluchnast.mushrooms.friends.data.db.Friend

class FriendsViewModel @ViewModelInject constructor(
    private val friendsRepository: FriendsRepository,
) : ViewModel() {

    suspend fun liveFriends() = friendsRepository.getAllFriends()

    fun addFriend(friend: NewFriendBtResult) {
        viewModelScope.launch {
            Log.i("FRIENDS VM", "persistFriend $friend")
            val newFriend = Friend(friend.username, friend.avatar, friend.isRemote, friend.remoteAddress, friend.publicKey)
            runCatching {
                friendsRepository.addFriend(newFriend, friend.myPassword!!, friend.remotePassword!!)
            }.onFailure {
                Log.e("FRIENDS VM", "db error $it")
            }
        }
    }
}