package pl.kamiluchnast.mushrooms.friends.addfriend

import android.os.Parcel
import android.os.Parcelable

data class NewFriendBtResult @JvmOverloads constructor(
    val username: String = "",
    val avatar: String? = null,
    val isRemote: Boolean = false,
    val remoteAddress: String? = null,
    val myPassword: String? = null,
    val remotePassword: String? = null,
    val publicKey: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(username)
        parcel.writeString(avatar)
        parcel.writeByte(if (isRemote) 1 else 0)
        parcel.writeString(remoteAddress)
        parcel.writeString(myPassword)
        parcel.writeString(remotePassword)
        parcel.writeString(publicKey)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewFriendBtResult> {
        override fun createFromParcel(parcel: Parcel): NewFriendBtResult {
            return NewFriendBtResult(parcel)
        }

        override fun newArray(size: Int): Array<NewFriendBtResult?> {
            return arrayOfNulls(size)
        }
    }
}