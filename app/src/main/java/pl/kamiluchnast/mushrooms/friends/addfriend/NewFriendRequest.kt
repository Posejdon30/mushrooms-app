package pl.kamiluchnast.mushrooms.friends.addfriend

import pl.kamiluchnast.mushrooms.friends.data.db.Friend

data class NewFriendRequest(
    val username: String,
    val isRemote: Boolean,
    val remoteAddress: String? = null,
    val myPassword: String? = null,
    val remotePassword: String? = null,
    val publicKey: String? = null
) {

    constructor(friend: Friend, myPassword: String, remotePassword: String) : this(
        friend.username,
        friend.isRemote,
        friend.remoteAddress,
        myPassword,
        remotePassword,
        friend.publicKey
    )
}
