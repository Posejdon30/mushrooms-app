package pl.kamiluchnast.mushrooms.friends.addfriend

data class NewFriendResponse(val status: String)
