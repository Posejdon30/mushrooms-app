package pl.kamiluchnast.mushrooms.friends.data

import android.util.Log
import androidx.lifecycle.LiveData
import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import pl.kamiluchnast.mushrooms.friends.data.db.FriendsLocalDataSource
import pl.kamiluchnast.mushrooms.friends.data.web.FriendsRemoteDataSource
import pl.kamiluchnast.mushrooms.utils.Result.Error
import pl.kamiluchnast.mushrooms.utils.Result.Success
import javax.inject.Inject

class FriendsRepository @Inject constructor(
    private val localDb: FriendsLocalDataSource,
    private val remoteApi: FriendsRemoteDataSource,
) {

    // This method save friend in both sources, remote server and local db
    suspend fun addFriend(friend: Friend, myPassword: String, remotePassword: String) {
        Log.i("FRIENDS REPO", "saving friend to local db")
        localDb.insertFriend(friend)
        Log.i("FRIENDS REPO", "sending friend to server")
        remoteApi.saveFriend(friend, myPassword, remotePassword)
    }

    suspend fun getAllFriends(forceUpdate: Boolean = false): LiveData<List<Friend>> {
        if (forceUpdate) {
            try {
                updateFriendsFromWebDataSource()
            } catch (e: Exception) {
                Log.e("FRIENDS REPOSITORY", "error in getting all friends", e)
            }
        }
        return localDb.getAllFriends()
    }

    private suspend fun updateFriendsFromWebDataSource() {
        val remoteFriends = remoteApi.getFriends()
        if (remoteFriends is Success) {
            remoteFriends.data.forEach { friend ->
                localDb.insertFriend(friend)
            }
        } else if (remoteFriends is Error) {
            throw remoteFriends.exception
        }
    }
}