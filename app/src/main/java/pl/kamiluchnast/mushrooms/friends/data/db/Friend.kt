package pl.kamiluchnast.mushrooms.friends.data.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "friends")
data class Friend constructor(
    @PrimaryKey @ColumnInfo(name = "username") val username: String = "",
    @ColumnInfo(name = "avatar") val avatar: String? = null,
    @ColumnInfo(name = "is_remote") val isRemote: Boolean = false,
    @ColumnInfo(name = "remote_address") val remoteAddress: String? = null,
    @ColumnInfo(name = "public_key") val publicKey: String? = null // @FIXME to powinno być obiektem PublicKey w encji
)