package pl.kamiluchnast.mushrooms.friends.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface FriendsDao {

    @Query("SELECT * FROM friends")
    fun getAllFriends(): LiveData<List<Friend>>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertFriend(friend: Friend)

    @Query("SELECT * FROM friends WHERE username = :username")
    suspend fun getFriend(username: String): Friend

    @Query("DELETE FROM friends WHERE username = :username")
    suspend fun deleteFriendByUsername(username: String): Int
}
