package pl.kamiluchnast.mushrooms.friends.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

// @TODO set exportSchema to true on PROD
@Database(entities = [Friend::class], version = 1, exportSchema = false)
abstract class FriendsDatabase : RoomDatabase() {

    abstract fun friendDao(): FriendsDao
}