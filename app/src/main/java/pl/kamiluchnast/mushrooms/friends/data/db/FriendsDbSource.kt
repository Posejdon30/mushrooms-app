package pl.kamiluchnast.mushrooms.friends.data.db

import javax.inject.Inject

class FriendsDbSource @Inject constructor(
    private val friendsDao: FriendsDao
) : FriendsLocalDataSource {

    override fun getAllFriends() = friendsDao.getAllFriends()

    override suspend fun insertFriend(friend: Friend) = friendsDao.insertFriend(friend)

    override suspend fun deleteFriend(username: String): Int = friendsDao.deleteFriendByUsername(username)

    override suspend fun getFriend(username: String): Friend = friendsDao.getFriend(username)


}