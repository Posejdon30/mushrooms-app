package pl.kamiluchnast.mushrooms.friends.data.db

import androidx.lifecycle.LiveData

interface FriendsLocalDataSource {

    fun getAllFriends(): LiveData<List<Friend>>
    suspend fun insertFriend(friend: Friend)
    suspend fun deleteFriend(username: String): Int
    suspend fun getFriend(username: String): Friend
}