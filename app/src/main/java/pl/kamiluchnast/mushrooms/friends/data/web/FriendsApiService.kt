package pl.kamiluchnast.mushrooms.friends.data.web

import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendRequest
import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendResponse
import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import retrofit2.Call
import retrofit2.http.*

interface FriendsApiService {

    @GET("/api/friends")
    suspend fun getMyFriends(): List<Friend>

    @POST("/api/friends")
    suspend fun addNewFriend(@Body request: NewFriendRequest): NewFriendResponse

    @DELETE("/api/friends/{username}")
    suspend fun deleteFriend(@Path("username") username: String)
}