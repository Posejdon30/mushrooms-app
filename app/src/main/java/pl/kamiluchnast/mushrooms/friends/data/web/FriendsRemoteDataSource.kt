package pl.kamiluchnast.mushrooms.friends.data.web

import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import pl.kamiluchnast.mushrooms.utils.Result

interface FriendsRemoteDataSource {

    suspend fun getFriends(): Result<List<Friend>>
    suspend fun saveFriend(friend: Friend, myPassword: String, remotePassword: String)
    suspend fun deleteFriend(username: String)
}