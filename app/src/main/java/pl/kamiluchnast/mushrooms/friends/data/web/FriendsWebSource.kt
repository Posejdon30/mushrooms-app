package pl.kamiluchnast.mushrooms.friends.data.web

import android.util.Log
import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendRequest
import pl.kamiluchnast.mushrooms.friends.data.db.Friend
import pl.kamiluchnast.mushrooms.utils.Result
import java.io.IOException
import javax.inject.Inject

class FriendsWebSource @Inject constructor(
    private val api: FriendsApiService,
) : FriendsRemoteDataSource {

    override suspend fun getFriends(): Result<List<Friend>> =
        try {
            Result.Success(api.getMyFriends())
        } catch (ex: IOException) {
            Result.Error(ex)
        }

    override suspend fun saveFriend(friend: Friend, myPassword: String, remotePassword: String) {
        Log.i("FRIENDS WEB SOURCE", "api call with $friend")
        api.addNewFriend(NewFriendRequest(friend, myPassword, remotePassword))
    }

    override suspend fun deleteFriend(username: String) {
        api.deleteFriend(username)
    }
}