package pl.kamiluchnast.mushrooms.friends.model

import pl.kamiluchnast.mushrooms.conversation.data.db.Message
import pl.kamiluchnast.mushrooms.friends.data.db.Friend

data class FriendListItem(val friend: Friend)
