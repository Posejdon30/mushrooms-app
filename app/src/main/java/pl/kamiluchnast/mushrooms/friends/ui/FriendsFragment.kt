package pl.kamiluchnast.mushrooms.friends.ui

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.databinding.FragmentFriendsBinding
import pl.kamiluchnast.mushrooms.friends.FriendsViewModel
import pl.kamiluchnast.mushrooms.friends.addfriend.NewFriendBtResult
import pl.kamiluchnast.mushrooms.utils.CryptoUtils
import javax.inject.Inject

@AndroidEntryPoint
class FriendsFragment : Fragment() {

    @Inject
    lateinit var crypto: CryptoUtils

    private lateinit var binding: FragmentFriendsBinding
    private lateinit var navController: NavController

    private val model: FriendsViewModel by viewModels()
    private val listAdapter: FriendsListAdapter = FriendsListAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFriendsBinding.inflate(inflater, container, false)
        context ?: return binding.root
        navController = findNavController()
        prepareFriendslist()
        prepareNewFriendButton()
        return binding.root
    }

    private fun prepareFriendslist() {
        binding.friendlist.apply {
            setHasFixedSize(true)
            adapter = listAdapter
        }
        lifecycleScope.launch {
            model.liveFriends().observe(viewLifecycleOwner, {
                Log.e("FRIENDS ACT", "observe functions has new friends for you: $it")
                listAdapter.submitList(it)
            })
        }
    }

    private fun prepareNewFriendButton() {
        binding.addNewFriend.setOnClickListener {

            AlertDialog.Builder(context)
                .setTitle("Nowy przyjaciel")
                .setMessage("Test?")
                .setPositiveButton("TAK") { _, _ ->
                    model.addFriend(NewFriendBtResult(
                        username = "Kamil",
                        avatar = "---image data here---",
                        isRemote = false,
//                        remoteAddress = "http://10.0.2.2:8080",
                        myPassword = "password-123",
                        remotePassword = "password-123",
                        publicKey = crypto.keyPair.public.toString()))
                }.setNegativeButton("NIE") { _, _ ->
                    if (!BluetoothAdapter.getDefaultAdapter().isEnabled) {
                        navController.navigate(R.id.action_friends_to_bluetooth)
                    }
                }.show()
        }
    }
}

//class TurnOnBluetooth : ActivityResultContract<Int, Boolean>() {
//    override fun createIntent(context: Context, ringtoneType: Int) =
//        Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE).apply {
//            putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 120)
//        }
//
//    override fun parseResult(resultCode: Int, result: Intent?) = (resultCode == Activity.RESULT_OK)
//}
