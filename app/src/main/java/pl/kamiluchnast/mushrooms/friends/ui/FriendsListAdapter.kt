package pl.kamiluchnast.mushrooms.friends.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.friends.data.db.Friend

class FriendsListAdapter : ListAdapter<Friend, FriendsListAdapter.FriendViewHolder>(COMPARATOR) {

    class FriendViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val idView: TextView = itemView.findViewById(R.id.friend_username)
        val isFriendRemote: CheckBox = itemView.findViewById(R.id.is_friend_remote)
        val remoteAddress: TextView = itemView.findViewById(R.id.remote_address)

        override fun toString(): String {
            return super.toString() + " '" + idView.text + "' remote: $isFriendRemote, address: $remoteAddress"
        }

        companion object {
            fun create(parent: ViewGroup): FriendViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.friend_item, parent, false)
                view.setOnClickListener {
                    val directions = FriendsFragmentDirections.actionFriendsToMessages(view.findViewById<TextView>(R.id.friend_username).text.toString())
                    view.findNavController().navigate(directions)
                }
                return FriendViewHolder(view)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FriendViewHolder {
        return FriendViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: FriendViewHolder, position: Int) {
        val item = getItem(position)
        holder.idView.text = item.username
        holder.isFriendRemote.isChecked = item.isRemote
        holder.remoteAddress.text = item.remoteAddress
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Friend>() {
            override fun areItemsTheSame(oldItem: Friend, newItem: Friend): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Friend, newItem: Friend): Boolean {
                return oldItem.username == newItem.username
            }
        }
    }
}