package pl.kamiluchnast.mushrooms.passwords

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.databinding.FragmentPasswordsBinding

@AndroidEntryPoint
class PasswordsFragment : Fragment() {

    private lateinit var binding: FragmentPasswordsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = setContentView(requireActivity().parent, R.layout.fragment_passwords)
        return binding.root
    }
}