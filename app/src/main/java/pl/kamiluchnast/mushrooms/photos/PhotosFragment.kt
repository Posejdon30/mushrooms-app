package pl.kamiluchnast.mushrooms.photos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import pl.kamiluchnast.mushrooms.R
import pl.kamiluchnast.mushrooms.databinding.FragmentPhotosBinding

@AndroidEntryPoint
class PhotosFragment : Fragment() {
    private lateinit var binding: FragmentPhotosBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.setContentView(requireActivity().parent, R.layout.fragment_photos)
        return binding.root
    }
}