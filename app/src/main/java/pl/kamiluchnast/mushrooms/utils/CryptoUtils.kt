package pl.kamiluchnast.mushrooms.utils

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Base64
import android.util.Log
import java.security.*
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher

class CryptoUtils {

    lateinit var keyPair: KeyPair

    companion object {
        const val AES_NOPAD_TRANS = "RSA/ECB/PKCS1Padding"
        const val ANDROID_KEYSTORE = "AndroidKeyStore"
        const val KEY_ALIAS = "mushrooms-rsa-keys"
    }

    init {
        if (!checkKeyExists()) {
            generateKey()
        }
    }

    private fun generateKey() {
        val startDate = GregorianCalendar()
        val endDate = GregorianCalendar()
        endDate.add(Calendar.YEAR, 100)
        val keyPairGenerator: KeyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_RSA, ANDROID_KEYSTORE)

        keyPairGenerator.initialize(keySpecs())
        keyPair = keyPairGenerator.genKeyPair()
    }

    private fun keySpecs() =
        KeyGenParameterSpec.Builder(KEY_ALIAS,
            KeyProperties.PURPOSE_DECRYPT or KeyProperties.PURPOSE_ENCRYPT).run {
            setDigests(KeyProperties.DIGEST_SHA256)                         //Set of digests algorithms with which the key can be used
            setSignaturePaddings(KeyProperties.SIGNATURE_PADDING_RSA_PKCS1) //Set of padding schemes with which the key can be used when signing/verifying
            build()
        }

    private fun createAsymmetricKeyPair(): KeyPair {
        val generator: KeyPairGenerator = KeyPairGenerator.getInstance("RSA")
        generator.initialize(2048)
        return generator.generateKeyPair()
    }

    private fun checkKeyExists(): Boolean {
        val keyStore: KeyStore = KeyStore.getInstance(ANDROID_KEYSTORE).apply {
            load(null)
        }
        val privateKey: PrivateKey? = keyStore.getKey(KEY_ALIAS, null) as PrivateKey?
        val publicKey: PublicKey? = keyStore.getCertificate(KEY_ALIAS)?.publicKey
        return privateKey != null && publicKey != null
    }

    private fun createKeyStore(): KeyStore {
        val keyStore = KeyStore.getInstance(ANDROID_KEYSTORE)
        keyStore.load(null)
        return keyStore
    }

    fun getAsymmetricKeyPair(): KeyPair? {
        val keyStore: KeyStore = createKeyStore()
        return try {
            val privateKey = keyStore.getKey(KEY_ALIAS, null) as PrivateKey?
            val publicKey = keyStore.getCertificate(KEY_ALIAS)?.publicKey
            return if (privateKey != null && publicKey != null) {
                KeyPair(publicKey, privateKey)
            } else {
                createAsymmetricKeyPair()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            createAsymmetricKeyPair()
        }
    }

    fun removeKeyStoreKey() = createKeyStore().deleteEntry(KEY_ALIAS)

    fun parsePublicKey(publicKey: String): PublicKey? {
        try {
            return KeyFactory.getInstance("RSA")
                .generatePublic(
                    X509EncodedKeySpec(
                        publicKey.toByteArray()
                    )
                )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    fun encryptWithKey(data: String, key: Key?): String {
        Log.w("CRYPTO", "encrypting, data: $data, key: $key")
        val cipher: Cipher = Cipher.getInstance(AES_NOPAD_TRANS)
        cipher.init(Cipher.ENCRYPT_MODE, key)
        val bytes = cipher.doFinal(data.toByteArray())
        return Base64.encodeToString(bytes, Base64.DEFAULT)
    }

    fun decrypt(data: String): String {
        val cipher: Cipher = Cipher.getInstance(AES_NOPAD_TRANS)
        cipher.init(Cipher.DECRYPT_MODE, keyPair.private)
        val encryptedData = Base64.decode(data, Base64.DEFAULT)
        val decodedData = cipher.doFinal(encryptedData)
        return String(decodedData)
    }

    fun decryptWithKey(data: String, key: Key?): String {
        val cipher: Cipher = Cipher.getInstance(AES_NOPAD_TRANS)
        cipher.init(Cipher.DECRYPT_MODE, key)
        val encryptedData = Base64.decode(data, Base64.DEFAULT)
        val decodedData = cipher.doFinal(encryptedData)
        return String(decodedData)
    }
}