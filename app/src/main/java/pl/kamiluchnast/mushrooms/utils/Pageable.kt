package pl.kamiluchnast.mushrooms.utils

data class Pageable<T>(
    val content: List<T>, val pageable: PageableParams, val totalPages: Int,
    val last: Boolean, val totalElements: Int, val size: Int, val sort: SortParams, val numberOfElements: Int,
    val first: Boolean, val number: Int, val empty: Boolean,
)

data class PageableParams(
    val sort: SortParams, val offset: Int, val pageNumber: Int,
    val pageSize: Int, val paged: Boolean, val unpaged: Boolean,
)

data class SortParams(val unsorted: Boolean, val sorted: Boolean, val empty: Boolean)
