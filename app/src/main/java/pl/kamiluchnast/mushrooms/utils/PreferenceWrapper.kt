package pl.kamiluchnast.mushrooms.utils

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import javax.inject.Inject

class PreferenceWrapper @Inject constructor(context: Context) {

    private var appContext: Context = context
    var username: String?
    var server: String
    var authToken: String?

    init {
        val userPrefs = appContext.getSharedPreferences("user_prefs", AppCompatActivity.MODE_PRIVATE)
        this.username = userPrefs.getString("username", null)
        this.authToken = userPrefs.getString("auth_token", "Bearer init-empty")
        this.server = userPrefs.getString("server", "http://10.0.2.2:8080")!!
    }

    fun saveLoginResponse(username: String, authToken: String, server: String) {
        this.username = username
        this.server = server
        this.authToken = "Bearer $authToken"
        appContext.getSharedPreferences("user_prefs", AppCompatActivity.MODE_PRIVATE).edit(true) {
            putString("username", username)
            putString("auth_token", "Bearer $authToken")
            putString("server", server)
        }
    }
}