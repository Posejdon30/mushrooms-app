package pl.kamiluchnast.mushrooms.utils

import android.util.Base64
import org.json.JSONException
import org.json.JSONObject
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime


class TokenUtils {

    fun isTokenOk(token: String?) = token != null && token.isNotBlank() && !isTokenExpired(token)

    private fun isTokenExpired(token: String) = try {
        val expireAt = decodeToken(token).getLong("exp")
        ZonedDateTime.now().isBefore(Instant.ofEpochMilli(expireAt).atZone(ZoneId.systemDefault()))
    } catch (je: JSONException) {
        true
    } catch (iae: IllegalArgumentException) {
        true
    }

    private fun decodeToken(token: String) = JSONObject(
        token.substringAfter(".")
            .substringBefore(".")
            .base64Decode()
    )

    private fun String.base64Decode() =
        Base64.decode(this, Base64.DEFAULT).toString(charset(Charsets.UTF_8.name()))
}